# Latext 数学公式语法

参考资料：

https://www.latex-project.org/help/documentation/amsldoc.pdf


# 数学符号参数

eg: \int_{a \to b}, `\int` 为积分符号, 加上`_{}` 表示该运算符的参数

## 数学公式

* 跨行数学公式使用 `$$ ... $$` 
    * 使用 `\\` 对多行公式进行换行
* 单个公式 `$ ... $`
* 为数学公式添加编号

$$
\begin{align}
a = b
&& \text{1.1}
\end{align}
$$



