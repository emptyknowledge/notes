# Calculus

# word

* arbitrary

adj 任意的；武断的；随心所欲的；专横的

* analytical

adj 分析的；解析的；分析性的；（科学）分析的

* constant

adj 恒定不变的；继续不断的；忠实的；坚韧的
n 【数,物】常数；恒定(值)；(常)系数；【语】(转换语法用语)定项

* curve

n 曲线；弯曲；弯曲物；曲线规
v 弄弯；使弯曲；成弯曲状；(依)曲线行进

* derivative

n 【数】导数；【化】衍生物；【语】派生词；派生物

adj 导出的，派生的

* determine

v 确定；决定；测定；查明

* formulate

v 制订；准备；规划；构想

* geometrical

adj 几何学的；按几何级数增长的

* sketch

n 素描；草图；速写；概述
v 概述；简述；画素描；画速写

* tangent

n 切线；正切
adj  【数】切线的；正切的；接触的；离题的

# phrase

* in term of

在某方面；是在学期中，按照

