#  Linear Algebra

# Words

* align

v 排列整齐；使对齐；（尤指）成一直线；使一致

* compress

v 压缩；使(文章等)变简练；经受压缩
n 敷；【医】压布；(棉花等的)打包机

* eigen

adj 特征的， 自身的
n 本征，特征

* elimination

n 消除；【数】消去；【化】弃置

* illustration

n 插图；图解；示例；图示

* immersive

adj （计算机系统或图像）沉浸式虚拟现实的

* interactive

adj 合作的；相互影响的；互相配合的；交互式的
n 交互式视频设备

* interpret

v 诠释；说明；口译；把…理解为

* mechanical

adj 机动的；机械驱动的；机械的；机器的

* parallel

adj 平行（的）；极相似的；同时发生的；相应的
v 与…相似；与…同时发生；与…媲美；比得上
n （尤指不同地点或时间的）极其相似的人（或情况、事件等）

* quadratic

n 【数】二次方程式；二次项；【数】二次方程式论
adj 二次方的，平方的

* segmentation

n 分割，分裂

* synthesize

v 合成；综合；用综合法处理；人工合成

* stability

n 稳定（性），稳固（性）